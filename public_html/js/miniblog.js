$(function () {
    var APPLICATION_ID = "B7F38D46-B26F-EAA7-FF5E-A33F4CAF5800",
       SECRET_KEY = "94299421-B470-993B-FF7E-9809D2952F00",
       VERSION = "v1";
  
     Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
     
    var postsCollection = Backendless.Persistence.of(Posts).find();
   
    console.log(postsCollection);
      
    var wrapper = {
        posts: postsCollection.data
    };
    
    Handlebars.registerHelper('format', function(time){
        return moment(time).format("dddd, MMMM Do YYYY");
    })
       
    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHTML = blogTemplate(wrapper);
       
    $('.main-container').html(blogHTML);
       
});             

 function Posts(args) {
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
 }  