$(function () {
    var APPLICATION_ID = "B7F38D46-B26F-EAA7-FF5E-A33F4CAF5800",
       SECRET_KEY = "94299421-B470-993B-FF7E-9809D2952F00",
       VERSION = "v1";
  
     Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
     
   
       
    var loginScript = $("#login-template").html();
    var loginTemplate = Handlebars.compile(loginScript);
   
    $('.main-container').html(loginTemplate);
    
    $(document).on('submit', '.form-signin', function(event){
       event.preventDefault();
       
       var data = $(this).serializeArray(),
       email = data[0].value,
       password = data[1].value;
       
       Backendless.UserService.login(email, password, true, new Backendless.Async(userLoggedIn, gotError));
       
   
    });   
});             

 function Posts(args) {
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
 } 
 
 function userLoggedIn(){
     console.log("user logged in");
 }
                                                                                    
 function gotError(error) {
     console.log("Error message - " + error.message);
     console.log("Error code - " + error.code);
 }
 
 